package pl.akademiakodu.hospital.dao;

import org.springframework.data.repository.CrudRepository;
import pl.akademiakodu.hospital.model.Patient;
import pl.akademiakodu.hospital.model.Visit;

import java.util.List;

public interface VisitRepository extends CrudRepository<Visit, Long> {
    List<Visit> findByPatient(Patient patient);
}
