package pl.akademiakodu.hospital.dao;

import org.springframework.data.repository.CrudRepository;
import pl.akademiakodu.hospital.model.Doctor;
import pl.akademiakodu.hospital.model.Patient;

import java.util.List;

public interface PatientRepository extends CrudRepository<Patient, Long> {
    List<Patient> findByDoctor(Doctor doctor);
}
