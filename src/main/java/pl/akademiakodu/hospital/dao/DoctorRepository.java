package pl.akademiakodu.hospital.dao;

import org.springframework.data.repository.CrudRepository;
import pl.akademiakodu.hospital.model.Doctor;

public interface DoctorRepository extends CrudRepository<Doctor, Long> {
}
